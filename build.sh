#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build ioquake3
#
pushd "source"
make -j "$(nproc)"
popd

for app_id in $STEAM_APP_ID_LIST ; do
	COPYDIR="../$app_id/dist" make --directory="source" copyfiles
	mkdir -p "$app_id/dist/license/"
	cp -v "source/COPYING.txt" "$app_id/dist/license/LICENSE.ioquake3.txt"
done
