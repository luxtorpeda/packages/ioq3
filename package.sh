#!/bin/bash

source env.sh

set -x
set -e

# Create a tarball, in a reproducible way

create_dist_tar_xz () {
	tar \
		--format=v7 \
		--mode='a+rwX,o-w' \
		--owner=0 \
		--group=0 \
		--mtime="@$SOURCE_DATE_EPOCH" \
		-cf dist.tar \
		--files-from=-
	xz dist.tar
}

create_manifest () {
	local -r app_id=$1
	local -r source_desc=$(git_source_describe)
	local -r package_commit=$(git rev-parse HEAD)
	cp -f "manifests/$app_id.json" "$app_id/manifest.json"
	sed -i "s|%GIT_COMMIT%|$package_commit|" "$app_id/manifest.json"
	sed -i "s|%GIT_SOURCE_DESCRIPTION%|$source_desc|" "$app_id/manifest.json"
}

list_pkg_files () {
	echo manifest.json
	list_dist
}

for app_id in $STEAM_APP_ID_LIST ; do
	create_manifest "$app_id"
	pushd "$app_id"
	list_pkg_files | create_dist_tar_xz
	sha1sum dist.tar.xz
	popd
done
